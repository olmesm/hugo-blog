# Hugo Blog

Implementation of a [Hugo] blog using [One] theme.

Uses docker for local dev, with final deploy to gitlab pages.

Blog available on [ohmybuck.com].

## Requires

- [Docker]

## To Run

```sh
# Run Locally
sh scripts/run-dev.sh

# Dev server
hugo server --bind=0.0.0.0
# ... then visit http://localhost:1313

# Make a new post
# sh scripts/make-new.sh <NAME>
sh scripts/new.sh "My New Post Name"
```

## Custom CSS

Add any custom CSS to `src/static/css/custom.css`. When the blog is being built, it will automatically add the custom css within the header of the theme.

## Publish Site

Just push to master - gitlab will handle the rest.

<!-- References -->

[Hugo]: https://gohugo.io/
[One]: https://github.com/resugary/hugo-theme-one
[Docker]: https://www.docker.com/
[ohmybuck.com]: https://ohmybuck.com
