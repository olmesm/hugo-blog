#! /bin/sh

source $(dirname "${BASH_SOURCE[0]}")/env

# Check if container is running
if [ "$RUNNING_CONTAINER_ID" ]
then
  # Connect to running Container
  docker exec -it $RUNNING_CONTAINER_ID sh
else
  # Start a dev container
  docker run --rm -it \
    --name $CONTAINER_NAME \
    -p $PORT:$PORT \
    -v $LOCAL_DIR/src:$WORKDIR/src \
    -v $LOCAL_DIR/scripts:$WORKDIR/scripts \
    -v $LOCAL_DIR/public:$WORKDIR/public \
    -w $WORKDIR \
    python:alpine sh -c 'sh scripts/setup-container.sh && sh'
fi
