#!/bin/sh

HUGO_VERSION=0.46
CONFIG_FILE=src/config.toml
CONTENT_DIRECTORY=src/content
ASSSETS_DIRECTORY=src/static

# Install Deps
apk add --update curl git ca-certificates asciidoc
pip install pygments

# Install Hugo
curl -sL -o /tmp/hugo.tar.gz \
https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz && \
tar -xzf /tmp/hugo.tar.gz -C /bin

# Theme
git clone --depth=1 https://github.com/resugary/hugo-theme-one.git themes/one

# Remove Extra Packages
rm -rf /tmp/*

# Make config file if it doesnt exist
if [[ ! -e "$CONFIG_FILE" ]]; then
  cp src/_example.config.toml "$CONFIG_FILE"
  echo "> config file didn't exist... created!"
fi

# Symlink config and content
ln -s "$CONFIG_FILE" config.toml
ln -s "$CONTENT_DIRECTORY" content
ln -s "$ASSSETS_DIRECTORY" static

# Add custom css
sh scripts/add-custom-css.sh
