#!/bin/sh

# Makes a new post
#   sh scripts/new.sh "My New Post Name"

TYPE=post
NAME=$1
CONTENT_DIR=src/content/posts
DATE_FORMAT="%Y-%m-%d" # this is used in the template too
DATE=$(date +"$DATE_FORMAT")
TEMPLATE_TITLE="Post Name"

OUTPUT_NAME=$(echo "$NAME" | tr '[:upper:]' '[:lower:]' | sed 's/ /-/g')
FILE_NAME="$DATE-$OUTPUT_NAME.md"

sed "s|$DATE_FORMAT|$DATE|; s|$TEMPLATE_TITLE|$NAME|" "templates/$TYPE.md" > "$CONTENT_DIR/$FILE_NAME"
