#!/bin/sh

CUSTOM_CSS="<link rel=\"stylesheet\" type=\"text/css\" href=\"{{ .Site.BaseURL }}css/custom.css\">"
THEME_HEADER_LOCATION="themes/one/layouts/partials/header.html"

# sed -i "/$TEXT_TO_PREPEND/c\\$CUSTOM_CSS\n$TEXT_TO_PREPEND" "$THEME_HEADER_LOCATION"
sed -i "/.* type=\"text\/css\".*/ a $CUSTOM_CSS" "$THEME_HEADER_LOCATION"
